from django.db import models


# Create your models here.
class Roles(models.Model):
    """ 
    Class ini digunakan untuk mmemodelkan tabel Roles pada Database
    params:
        ID (int): role_id
        role (String): type of role    
    """
    ROLE_CHOICES = [
        ('pemilik', 'Pemilik'),
        ('peminjam', 'Peminjam')
    ]

    role = models.CharField(max_length=10, choices=ROLE_CHOICES, default='peminjam')

    def __str__(self):
        return self.role


class User(models.Model):
    """ 
    Class ini digunakan untuk mmemodelkan tabel User pada Database
    params:
        ID (int): user_id
        nama (String): nama user
        email (String): email user
        no_telpon(int): nomor telpon
        alamat(String): alamat user
        password(String): password user
        roles (String): role pada buku
    """
    nama = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    no_telpon = models.IntegerField()
    alamat = models.CharField(max_length=200)
    password = models.CharField(max_length=100)
    role_id = models.ForeignKey(Roles, on_delete=models.CASCADE)

    class Meta:
        ordering = ['nama', 'email']

    def __str__(self):
        return self.nama


class Genre(models.Model):
    """ ID, genre """
    GENRE_CHOICES = [
        ('pendidikan', 'Pendidikan'),
        ('sejarah', 'Sejarah'),
        ('majalah', 'Mendidikan')
    ]
    genre = models.CharField(max_length=30, choices=GENRE_CHOICES, blank=True)

    class Meta:
        ordering = ['genre']

    def __str__(self):
        return self.genre

class Books(models.Model):
    """ ID, genre_id, judul_buku, deskripsi, gambar, harga, pemilik_id, peminjam_id, status, publish """
    STATUS_CHOISES = [
        ('available', 'Tidak sedang dipinjam'),
        ('unavailable', 'Sedang dipinjam')
    ]

    genre_id = models.ForeignKey(Genre, on_delete=models.CASCADE)
    judul_buku = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=200, blank=True)
    gambar = models.ImageField(upload_to='books')
    harga = models.DecimalField(max_digits=10, decimal_places=2)
    pemilik_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pemilik_buku')
    peminjam_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='peminjam_buku')
    status = models.CharField(max_length=30, default='available')
    publish = models.CharField(max_length=35, blank=True)

    class Meta:
        ordering = ['judul_buku', 'status']

    def __str__(self):
        return self.judul_buku