from django.contrib import admin
from rental_buku.models import User, Books, Genre, Roles


# Register your models here.
admin.site.register([User, Books, Genre, Roles])