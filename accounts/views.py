from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.hashers import make_password, check_password

from rental_buku.models import Roles, User
from accounts.forms import RegisterForm, LoginForm


# Create your views here.
def register(request):
    """ Lakukan Register untuk User baru 
        params:
            nama(string): nama user
            email(string): email user
            no_telpon(int): Nomor telpon
            alamat(string): alamat user
            password(hashed string): Password yang diendkripsi
            role_id(int): ID yang menuju ke table Roles (digunakan untuk mengatur level setiap user)
                > 1. Pemilik
                > 2. Peminjam

    """
    if request.method == "POST":
        form = RegisterForm(request.POST, initial={'role_id': False})
        if form.is_valid():
            nama = form.cleaned_data['nama']
            email = form.cleaned_data['email']
            no_telpon = form.cleaned_data['no_telpon']
            alamat = form.cleaned_data['alamat']
            password = form.cleaned_data['password']
            role_id = form.cleaned_data['role_id']
            if form.cleaned_data['role_id'] == 'Pemilik':
                role_id = Roles.objects.get(pk=1)
            elif form.cleaned_data['role_id'] == 'Peminjam':
                role_id = Roles.objects.get(pk=2)

            new_user = User(
                nama=nama, email=email,
                no_telpon=no_telpon, alamat=alamat,
                password=make_password(password), role_id=role_id
                )

            new_user.save()

            messages.add_message(request, messages.INFO, 'Akun anda berhasil terdaftar', extra_tags='success')
            return redirect('register')
        elif not form.is_valid():
            messages.add_message(request, messages.ERROR, 'Semua fields wajib diisi!', extra_tags='danger')
            return redirect('register')
    else:
        form  = RegisterForm()

    return render(request, 'register.html', {'form': form})

def login(request):
    """ Melakukan login ke sistem dengan menggukana email dan passworf yang sesuai 
        params:
            email(string): email user
            password(hashed string): Password yang diendkripsi

        return:
            Pemilik:  Home
                      Buku saya
                      Pinjaman saya

            Peminjam: Home
                      Pinjaman saya
    """
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = User.objects.filter(email=email).first()
            if user:
                if check_password(password, user.password):
                    messages.add_message(request, messages.INFO, 'Login anda berhasil', extra_tags='success')
                    return redirect('home')
                
                messages.add_message(request, messages.ERROR, 'Password anda salah!', extra_tags='danger')
                return redirect('login')

            messages.add_message(request, messages.INFO, 'User tidak ditemukan!', extra_tags='danger')
            return redirect('login')

        else:
            messages.add_message(request, messages.ERROR, 'Semua fields wajib diisi!', extra_tags='danger')
            return redirect('login')
    else:
        form = LoginForm()

    return render(request, 'login.html', {'form': form})
    