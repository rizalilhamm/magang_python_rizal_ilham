from django import forms
from django.forms.widgets import EmailInput
from rental_buku.models import User


class RegisterForm(forms.Form):
    """
    nama, email, no_telpon, alamat, password, role_id 
    """
    nama = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Nama'}))
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'placeholder': 'Email'}))
    no_telpon = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'No. Telpon'}))
    alamat = forms.CharField(widget=forms.Textarea(attrs={'rows': 5, 'cols': 23, 'placeholder': 'Alamat'}))
    password = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    role_id = forms.ChoiceField(choices=(('Pemilik', 'Pemilik'), ('Peminjam', 'Peminjam')))

    class Meta:
        model = User
        fields = ('nama', 'email', 'no_telpon', 'alamat', 'password')


class LoginForm(forms.Form):
    email = forms.CharField(max_length=30, widget=forms.EmailInput(attrs={'placeholder': 'Nama'}))
    password = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))

    class Meta:
        model = User
        fields = ('email', 'password')